package com.kozu.grabitup.productDetails.model;

public class ColorModel {
    private String colorCode;

    public ColorModel(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }
}
