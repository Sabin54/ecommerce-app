package com.kozu.grabitup.productDetails.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.kozu.grabitup.R;
import com.kozu.grabitup.databinding.ActivityProductDetailsBinding;
import com.kozu.grabitup.myCarts.CartsView;
import com.kozu.grabitup.productDetails.adapter.ProductColorAdapter;
import com.kozu.grabitup.productDetails.model.ColorModel;
import com.kozu.grabitup.utility.GlobalValues;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProductDetailsView extends AppCompatActivity {
    ActivityProductDetailsBinding productDetailsBinding;
    List<ColorModel> colorModels = new ArrayList<>();
    Toolbar toolbar;
    TextView title;
    CircleImageView toolbarLogo;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewBinding();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        title = findViewById(R.id.title);   //ToolBar Title Custom
        toolbarLogo = findViewById(R.id.toolbarLogo);
        title.setText("Product Details");
        toolbarLogo.setImageResource(R.drawable.grabitup);

        dummyColor();
        ProductColorAdapter productColorAdapter = new ProductColorAdapter(colorModels,this);
        productDetailsBinding.rvColors.setAdapter(productColorAdapter);

        Handler mhandler = new Handler();
        mhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                productDetailsBinding.productImage.setBackgroundColor(Color.parseColor(GlobalValues.selectedColor));
                progressDialog.dismiss();
            }
        },3000);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductDetailsView.this, CartsView.class));
                finish();
            }
        });
    }

    private void initViewBinding()
    {
        productDetailsBinding = ActivityProductDetailsBinding.inflate(getLayoutInflater());
        setContentView(productDetailsBinding.getRoot());
    }

    public void onColorSelected(String color)
    {
        productDetailsBinding.productImage.setBackgroundColor(Color.parseColor(color));
    }

    private void dummyColor()
    {
        ColorModel colorModel = new ColorModel("#EB3829");
        colorModels.add(colorModel);

        colorModel = new ColorModel("#3AA33E");
        colorModels.add(colorModel);

        colorModel = new ColorModel("#FF6200EE");
        colorModels.add(colorModel);
    }
}
