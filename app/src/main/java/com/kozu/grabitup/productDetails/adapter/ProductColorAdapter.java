package com.kozu.grabitup.productDetails.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kozu.grabitup.databinding.RvProductsColorsBinding;
import com.kozu.grabitup.productDetails.model.ColorModel;
import com.kozu.grabitup.productDetails.view.ProductDetailsView;
import com.kozu.grabitup.utility.GlobalValues;

import java.util.List;

public class ProductColorAdapter extends RecyclerView.Adapter<ProductColorAdapter.myHolder> {
    List<ColorModel> colors;
    ProductDetailsView context;

    public ProductColorAdapter(List<ColorModel> colors, ProductDetailsView context) {
        this.colors = colors;
        this.context = context;
    }

    @NonNull
    @Override
    public myHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new myHolder(RvProductsColorsBinding.inflate(LayoutInflater.from(context),parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull myHolder holder, int position) {
        holder.itemView.availableColor.setBackgroundColor(Color.parseColor(colors.get(position).getColorCode()));
        GlobalValues.selectedColor = colors.get(0).getColorCode();
        holder.itemView.availableColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onColorSelected(colors.get(position).getColorCode());
            }
        });
    }

    @Override
    public int getItemCount() {
        return colors.size();
    }

    public class myHolder extends RecyclerView.ViewHolder {
        RvProductsColorsBinding itemView;
        public myHolder(@NonNull RvProductsColorsBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
