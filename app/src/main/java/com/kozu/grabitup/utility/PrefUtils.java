package com.kozu.grabitup.utility;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefUtils {
    private static SharedPreferences prefs;
    private static SharedPreferences.Editor editor;

    public static void saveIsLogin(Context context, boolean status) {
        editor = context.getSharedPreferences(Constants.LOGIN_PREF, Context.MODE_PRIVATE).edit();
        editor.putBoolean(Constants.LOGIN_PREF_DATA, status);
        editor.apply();
    }

    public static boolean isLogin(Context context) {
        prefs = context.getSharedPreferences(Constants.LOGIN_PREF, Context.MODE_PRIVATE);
        return prefs.getBoolean(Constants.LOGIN_PREF_DATA, false);
    }

    public static void saveUsername(Context context, String  name) {
        editor = context.getSharedPreferences("USER_PROFILE_NAME", Context.MODE_PRIVATE).edit();
        editor.putString("Username", name);
        editor.apply();
    }

    public static String getUsername(Context context) {
        prefs = context.getSharedPreferences("USER_PROFILE_NAME", Context.MODE_PRIVATE);
        return prefs.getString("Username", "");
    }

    public static void saveUserProfilePicURL(Context context, String  url) {
        editor = context.getSharedPreferences("USER_PROFILE", Context.MODE_PRIVATE).edit();
        editor.putString("URL", url);
        editor.apply();
    }

    public static String  getUserProfileURL(Context context) {
        prefs = context.getSharedPreferences("USER_PROFILE", Context.MODE_PRIVATE);
        return prefs.getString("URL", "");
    }

    public static void saveTheme(Context context, int  id) {
        editor = context.getSharedPreferences("USER_THEME", Context.MODE_PRIVATE).edit();
        editor.putInt("THEME", id);
        editor.apply();
    }

    public static int getTheme(Context context) {
        prefs = context.getSharedPreferences("USER_THEME", Context.MODE_PRIVATE);
        return prefs.getInt("THEME", 0);
    }
}
