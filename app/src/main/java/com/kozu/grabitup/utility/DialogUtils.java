package com.kozu.grabitup.utility;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

public class DialogUtils {
    public static void setDialogConfig(Activity context, Dialog dialog) {
        int orientation = context.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            Window dialogWindow = dialog.getWindow();
            WindowManager m = context.getWindowManager();
            Display d = m.getDefaultDisplay();
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            lp.width = d.getWidth() * 1;

        } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Window dialogWindow = dialog.getWindow();
            dialogWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager m = context.getWindowManager();
            Display d = m.getDefaultDisplay();
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            lp.width = (int) (d.getWidth() * 0.5);
        }
    }
}
