package com.kozu.grabitup.dashboard.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.facebook.login.LoginManager;
import com.kozu.grabitup.R;
import com.kozu.grabitup.dashboard.adapter.ItemCategoryAdapter;
import com.kozu.grabitup.dashboard.view.DashboardView;
import com.kozu.grabitup.databinding.FragmentHomeViewBinding;
import com.kozu.grabitup.databinding.FragmentSettingViewBinding;
import com.kozu.grabitup.utility.DialogUtils;
import com.kozu.grabitup.utility.PrefUtils;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingFragment extends Fragment {
    FragmentSettingViewBinding settingFrag;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        settingFrag = FragmentSettingViewBinding.inflate(inflater,container,false);
        return settingFrag.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initClick();
        if (!PrefUtils.isLogin(getActivity())) {
            settingFrag.logout.setVisibility(View.GONE);
            settingFrag.changePassword.setVisibility(View.GONE);
        }else
        {
            settingFrag.logout.setVisibility(View.VISIBLE);
            settingFrag.changePassword.setVisibility(View.VISIBLE);
        }

    }

    public void initClick()
    {
        settingFrag.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PrefUtils.saveUserProfilePicURL(getActivity(),"");
                PrefUtils.saveIsLogin(getActivity(),false);
                PrefUtils.saveUsername(getActivity(),"");
                LoginManager.getInstance().logOut();
                startActivity(new Intent(getActivity(), DashboardView.class));
            }
        });
        settingFrag.aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingFrag.aboutUsDetails.setVisibility(View.VISIBLE);
            }
        });
        settingFrag.contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingFrag.contactUsDetails.setVisibility(View.VISIBLE);
            }
        });

       settingFrag.changeTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                themeChangeDialog();
            }
        });

    }

    private void themeChangeDialog()
    {
        Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_change_theme);
        RadioGroup theme_group = dialog.findViewById(R.id.theme_group);

        RadioButton light_theme = dialog.findViewById(R.id.light_theme);
        RadioButton dark_theme = dialog.findViewById(R.id.dark_theme);

       if (PrefUtils.getTheme(getActivity())==1)
       {
           dark_theme.setChecked(true);
       }else
       {
           light_theme.setChecked(true);
       }

        ImageView theme_close_dialog = dialog.findViewById(R.id.close_theme_dialog);

        theme_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.light_theme)
                {
                    PrefUtils.saveTheme(getActivity(),0);

                }else
                {
                    PrefUtils.saveTheme(getActivity(),1);

                }
                dialog.dismiss();
                onDestroy();
                getActivity().recreate();
            }
        });

        theme_close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        DialogUtils.setDialogConfig(getActivity(),dialog);
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!PrefUtils.isLogin(getActivity())) {
            settingFrag.logout.setVisibility(View.GONE);
            settingFrag.changePassword.setVisibility(View.GONE);
        }else
        {
            settingFrag.logout.setVisibility(View.VISIBLE);
            settingFrag.changePassword.setVisibility(View.VISIBLE);
        }
    }

}
