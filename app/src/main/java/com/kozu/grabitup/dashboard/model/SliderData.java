package com.kozu.grabitup.dashboard.model;

public class SliderData {
    private int imgUrl;

    public SliderData(int imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(Integer imgUrl) {
        this.imgUrl = imgUrl;
    }
}
