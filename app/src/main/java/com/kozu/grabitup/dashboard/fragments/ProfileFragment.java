package com.kozu.grabitup.dashboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.kozu.grabitup.databinding.FragmentProfileViewBinding;
import com.kozu.grabitup.loginOrSignUp.LoginAndSignup;
import com.kozu.grabitup.utility.PrefUtils;
import com.squareup.picasso.Picasso;

public class ProfileFragment extends Fragment {
    FragmentProfileViewBinding profileFrag;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        profileFrag = FragmentProfileViewBinding.inflate(inflater,container,false);
        return profileFrag.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (PrefUtils.isLogin(getActivity()))
        {
            profileFrag.loginView.setVisibility(View.GONE);
            profileFrag.profileView.setVisibility(View.VISIBLE);
            profileFrag.userEmail.setText(PrefUtils.getUsername(getActivity()));
            Picasso.get().load(PrefUtils.getUserProfileURL(getActivity())).into(profileFrag.profiePic);

        }else
        {
            profileFrag.loginView.setVisibility(View.VISIBLE);
            profileFrag.profileView.setVisibility(View.GONE);
        }


    profileFrag.loginSignup.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(getActivity(), LoginAndSignup.class));
        }
    });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (PrefUtils.isLogin(getActivity()))
        {
            profileFrag.loginView.setVisibility(View.GONE);
            profileFrag.profileView.setVisibility(View.VISIBLE);
            profileFrag.userEmail.setText(PrefUtils.getUsername(getActivity()));
            Picasso.get().load(PrefUtils.getUserProfileURL(getActivity())).fit().into(profileFrag.profiePic);
        }else
        {
            profileFrag.loginView.setVisibility(View.VISIBLE);
            profileFrag.profileView.setVisibility(View.GONE);
        }

    }
}
