package com.kozu.grabitup.dashboard.model;

public class ItemCategoryModel {
    private String categoryName;
    private String categoryId;
    private String categoryImage;

    public ItemCategoryModel(String categoryName, String categoryId, String categoryImage) {
        this.categoryName = categoryName;
        this.categoryId = categoryId;
        this.categoryImage = categoryImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }
}
