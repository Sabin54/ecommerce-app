package com.kozu.grabitup.dashboard.view;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.StyleRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationBarView;
import com.kozu.grabitup.R;
import com.kozu.grabitup.dashboard.fragments.Home;
import com.kozu.grabitup.dashboard.fragments.ProfileFragment;
import com.kozu.grabitup.dashboard.fragments.SettingFragment;
import com.kozu.grabitup.dashboard.fragments.WishLstFragment;
import com.kozu.grabitup.dashboard.model.ItemCategoryModel;
import com.kozu.grabitup.databinding.ActivityDashboardViewBinding;
import com.kozu.grabitup.myCarts.CartsView;
import com.kozu.grabitup.utility.PrefUtils;

import java.util.ArrayList;
import java.util.List;

public class DashboardView extends AppCompatActivity {
    ActivityDashboardViewBinding homepage;

    Toolbar toolbar;
    TextView title;

  //  FragmentManager fm = getSupportFragmentManager();
   Fragment fragment1,fragment2,fragment3,fragment4;
 /*   Fragment fragment1 = new Home();
    Fragment fragment2 = new SettingFragment();
    Fragment fragment3 = new WishLstFragment();
    Fragment fragment4 = new ProfileFragment();*/
    Fragment active;
    FragmentTransaction ft;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (PrefUtils.getTheme(this)==1)
        {
            getTheme().applyStyle(R.style.MyDarkTheme, true);
        }else
        {
            getTheme().applyStyle(R.style.BaseTheme, true);
        }
        super.onCreate(savedInstanceState);
        initViewBinding();

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        setSupportActionBar(toolbar);

        title = findViewById(R.id.title);   //ToolBar Title Custom

        ft = getSupportFragmentManager().beginTransaction();
        active = new Home();
        ft.replace(R.id.main_container, active);
        ft.commit();

        /*fm.beginTransaction().add(R.id.main_container, fragment4, "4").hide(fragment4).commit();
        fm.beginTransaction().add(R.id.main_container, fragment3, "3").hide(fragment3).commit();
        fm.beginTransaction().add(R.id.main_container, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.main_container,fragment1, "1").commit();*/
        title.setText("Home");// setting ToolBar Title

        if (savedInstanceState!=null)
        {
            ft = getSupportFragmentManager().beginTransaction();
            active = new SettingFragment();
            ft.replace(R.id.main_container, active);
            ft.commit();
            title.setText("Settings"); // setting ToolBar Title
        }
        homepage.customBottomBar.inflateMenu(R.menu.bottom_nav_menu);
        bottomNavigationClickListener();

        initClickListener();

    }

    private void initClickListener()
    {
        homepage.myCarts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardView.this, CartsView.class));
            }
        });
    }


    private void initViewBinding()
    {
        homepage = ActivityDashboardViewBinding.inflate(getLayoutInflater());
        setContentView(homepage.getRoot());
    }

    private void bottomNavigationClickListener()
    {
        homepage.customBottomBar.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId())
                {
                    case R.id.home:
                    {
                        homepage.customBottomBar.getMenu().findItem(R.id.home).setChecked(true);
                        ft = getSupportFragmentManager().beginTransaction();
                        active = new Home();
                        ft.replace(R.id.main_container, active);
                        ft.commit();
                        title.setText("Home");
                    }break;
                    case R.id.settings:
                    {
                        homepage.customBottomBar.getMenu().findItem(R.id.settings).setChecked(true);
                        ft = getSupportFragmentManager().beginTransaction();
                        active = new SettingFragment();
                        ft.replace(R.id.main_container, active);
                        ft.commit();
                        title.setText("Settings");
                    }break;
                    case R.id.wishlist:
                    {
                        homepage.customBottomBar.getMenu().findItem(R.id.wishlist).setChecked(true);
                        ft = getSupportFragmentManager().beginTransaction();
                        active = new WishLstFragment();
                        ft.replace(R.id.main_container, active);
                        ft.commit();
                        title.setText("WishLists");
                    }break;
                    case R.id.profile:
                    {
                        homepage.customBottomBar.getMenu().findItem(R.id.profile).setChecked(true);
                        ft = getSupportFragmentManager().beginTransaction();
                        active = new ProfileFragment();
                        ft.replace(R.id.main_container, active);
                        ft.commit();
                        title.setText("Profile");
                    }break;

                }
                return false;
            }
        });
    }

}