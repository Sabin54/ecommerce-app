package com.kozu.grabitup.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kozu.grabitup.dashboard.view.DashboardView;
import com.kozu.grabitup.databinding.RvMyCartsItemsBinding;
import com.kozu.grabitup.myCarts.CartsView;
import com.kozu.grabitup.myCarts.adapter.CartAdapter;
import com.kozu.grabitup.myCarts.model.CartData;

import java.util.List;

public class wishListAdapter extends RecyclerView.Adapter<wishListAdapter.myHolder> {
    List<CartData> cartData;
    Context context;

    public wishListAdapter(List<CartData> cartData, Context context) {
        this.cartData = cartData;
        this.context = context;
    }

    @NonNull
    @Override
    public myHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new myHolder( RvMyCartsItemsBinding.inflate(LayoutInflater.from(context),parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull myHolder holder, int position) {
        holder.itemView.setCartData(cartData.get(position));

        /*holder.itemView.cartData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onCartItemClicked();
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return cartData.size();
    }

    public class myHolder extends RecyclerView.ViewHolder {
        RvMyCartsItemsBinding itemView;
        public myHolder(@NonNull  RvMyCartsItemsBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}