package com.kozu.grabitup.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kozu.grabitup.dashboard.model.ItemCategoryModel;
import com.kozu.grabitup.databinding.RecyclerCategoryItemsBinding;

import java.util.List;

public class ItemCategoryAdapter extends RecyclerView.Adapter<ItemCategoryAdapter.myHolder> {
    Context context;
    List<ItemCategoryModel> categories;

    public ItemCategoryAdapter(Context context, List<ItemCategoryModel> categories) {
        this.context = context;
        this.categories = categories;
    }

    @NonNull
    @Override
    public myHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new myHolder( RecyclerCategoryItemsBinding.inflate(LayoutInflater.from(context),parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull myHolder holder, int position) {
        holder.itemView.setCategory(categories.get(position));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class myHolder extends RecyclerView.ViewHolder {
        RecyclerCategoryItemsBinding itemView;
        public myHolder(@NonNull  RecyclerCategoryItemsBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
