package com.kozu.grabitup.dashboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.kozu.grabitup.R;
import com.kozu.grabitup.dashboard.model.SliderData;
import com.kozu.grabitup.dashboard.adapter.ImageSliderAdapter;
import com.kozu.grabitup.dashboard.adapter.ItemCategoryAdapter;
import com.kozu.grabitup.dashboard.model.ItemCategoryModel;
import com.kozu.grabitup.databinding.FragmentHomeViewBinding;
import com.kozu.grabitup.utility.PrefUtils;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

public class Home extends Fragment {
    FragmentHomeViewBinding homeFragment;
    List<ItemCategoryModel> dummyDatas = new ArrayList<>();
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeFragment = FragmentHomeViewBinding.inflate(inflater,container,false);
        return homeFragment.getRoot();
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        initImageSlider();
        initDummyCategoryDatas();


        ItemCategoryAdapter itemCategoryAdapter = new ItemCategoryAdapter(getActivity(),dummyDatas);
        homeFragment.rvCategories.setAdapter(itemCategoryAdapter);

        homeFragment.rvItems.setAdapter(itemCategoryAdapter);
        homeFragment.rvMostPopular.setAdapter(itemCategoryAdapter);
        homeFragment.rvNewArrivals.setAdapter(itemCategoryAdapter);
    }

    private void initDummyCategoryDatas()
    {
        ItemCategoryModel itemCategoryModel = new ItemCategoryModel("Boys Fashion","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Girls Fashion","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Electronics","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Gents Shoes","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Girls Shoes","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Liquors","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Watches","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Laptops","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Girls Fashion","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Electronics","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Gents Shoes","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Girls Shoes","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Liquors","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Watches","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Laptops","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Girls Fashion","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Electronics","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Gents Shoes","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Girls Shoes","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Liquors","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Watches","","");
        dummyDatas.add(itemCategoryModel);
        itemCategoryModel = new ItemCategoryModel("Laptops","","");
        dummyDatas.add(itemCategoryModel);

    }

    private void initImageSlider()
    {
        ArrayList<SliderData> sliderDataArrayList = new ArrayList<>();

        // adding the urls inside array list
        sliderDataArrayList.add(new SliderData(R.drawable.image3));
        sliderDataArrayList.add(new SliderData(R.drawable.image1));
        sliderDataArrayList.add(new SliderData(R.drawable.image2));

        ImageSliderAdapter adapter = new ImageSliderAdapter(getActivity(),sliderDataArrayList);

        homeFragment.slider.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_LTR);

        homeFragment.slider.setSliderAdapter(adapter);

        homeFragment.slider.setScrollTimeInSec(3);

        homeFragment.slider.setAutoCycle(true);
        homeFragment.slider.startAutoCycle();
    }


}
