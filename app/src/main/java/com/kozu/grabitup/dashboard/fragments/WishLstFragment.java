package com.kozu.grabitup.dashboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.kozu.grabitup.dashboard.adapter.wishListAdapter;
import com.kozu.grabitup.databinding.FragmentSettingViewBinding;
import com.kozu.grabitup.databinding.FragmentWishlistViewBinding;
import com.kozu.grabitup.myCarts.CartsView;
import com.kozu.grabitup.myCarts.adapter.CartAdapter;
import com.kozu.grabitup.myCarts.model.CartData;

import java.util.ArrayList;
import java.util.List;

public class WishLstFragment extends Fragment {
    List<CartData> dummyDatas = new ArrayList<>();
    FragmentWishlistViewBinding wishlistFrag;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        wishlistFrag = FragmentWishlistViewBinding.inflate(inflater,container,false);
        return wishlistFrag.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        initDummyData();
        wishListAdapter cartAdapter = new wishListAdapter(dummyDatas, getActivity());
        wishlistFrag.rvWishLists.setAdapter(cartAdapter);
    }
    private void initDummyData()
    {
        CartData cartData = new CartData("","20kg dumbbells set","5000");
        dummyDatas.add(cartData);
        cartData = new CartData("","20kg dumbbells set","5000");
        dummyDatas.add(cartData);
        cartData = new CartData("","20kg dumbbells set","5000");
        dummyDatas.add(cartData);
        cartData = new CartData("","20kg dumbbells set","5000");
        dummyDatas.add(cartData);
        cartData = new CartData("","20kg dumbbells set","5000");
        dummyDatas.add(cartData);

    }
}
