package com.kozu.grabitup.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.kozu.grabitup.dashboard.model.SliderData;
import com.kozu.grabitup.databinding.RecyclerSliderViewBinding;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

    public class ImageSliderAdapter extends SliderViewAdapter<ImageSliderAdapter.SliderAdapterViewHolder> {
        private final List<SliderData> mSliderItems;
        Context context;
        public ImageSliderAdapter(Context context, ArrayList<SliderData> sliderDataArrayList) {
            this.mSliderItems = sliderDataArrayList;
            this.context = context;
        }

        @Override
        public SliderAdapterViewHolder onCreateViewHolder(ViewGroup parent) {
            return  new SliderAdapterViewHolder(RecyclerSliderViewBinding.inflate(LayoutInflater.from(context),parent,false));

        }

        @Override
        public void onBindViewHolder(SliderAdapterViewHolder viewHolder, final int position) {

            final SliderData sliderItem = mSliderItems.get(position);

            Glide.with(context).load(sliderItem.getImgUrl()).into(viewHolder.itemView.myImage);
        }

        @Override
        public int getCount() {
            return mSliderItems.size();
        }

        static class SliderAdapterViewHolder extends SliderViewAdapter.ViewHolder {
            RecyclerSliderViewBinding itemView;

            public SliderAdapterViewHolder(@NonNull RecyclerSliderViewBinding itemView) {
                super(itemView.getRoot());
                this.itemView = itemView;

            }
        }
    }