package com.kozu.grabitup.myCarts;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.kozu.grabitup.R;
import com.kozu.grabitup.databinding.ActivityMyCartsBinding;
import com.kozu.grabitup.myCarts.adapter.CartAdapter;
import com.kozu.grabitup.myCarts.model.CartData;
import com.kozu.grabitup.productDetails.view.ProductDetailsView;

import java.util.ArrayList;
import java.util.List;

public class CartsView extends AppCompatActivity {
    ActivityMyCartsBinding myCartsBinding;
    Toolbar toolbar;
    List<CartData> dummyDatas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewBinding();

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        dummyDatas = new ArrayList<>();
        initDummyData();
        CartAdapter cartAdapter = new CartAdapter(dummyDatas,CartsView.this);
        myCartsBinding.rvCarts.setAdapter(cartAdapter);

    }

    private void initDummyData()
    {
        CartData cartData = new CartData("","20kg dumbbells set","5000");
        dummyDatas.add(cartData);
        cartData = new CartData("","20kg dumbbells set","5000");
        dummyDatas.add(cartData);
        cartData = new CartData("","20kg dumbbells set","5000");
        dummyDatas.add(cartData);
        cartData = new CartData("","20kg dumbbells set","5000");
        dummyDatas.add(cartData);
        cartData = new CartData("","20kg dumbbells set","5000");
        dummyDatas.add(cartData);

    }

    public void onCartItemClicked()
    {
        startActivity(new Intent(this, ProductDetailsView.class));
    }


    private void initViewBinding()
    {
        myCartsBinding = ActivityMyCartsBinding.inflate(getLayoutInflater());
        setContentView(myCartsBinding.getRoot());
    }
}
