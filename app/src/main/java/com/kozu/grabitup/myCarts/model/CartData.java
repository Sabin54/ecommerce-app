package com.kozu.grabitup.myCarts.model;

public class CartData {
    public String image;
    public String productName;
    public String price;

    public CartData(String image, String productName, String price) {
        this.image = image;
        this.productName = productName;
        this.price = price;
    }
}
