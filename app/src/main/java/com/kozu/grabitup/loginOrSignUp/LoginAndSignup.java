package com.kozu.grabitup.loginOrSignUp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.gson.Gson;
import com.kozu.grabitup.databinding.LoginSignupPageBinding;
import com.kozu.grabitup.utility.CustomToast;
import com.kozu.grabitup.utility.GlobalValues;
import com.kozu.grabitup.utility.PrefUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class LoginAndSignup extends AppCompatActivity {
    LoginSignupPageBinding loginSignupPageBinding;
    CallbackManager callbackManager;
    ProgressDialog progressDialog;
    String username;
    GoogleSignInClient googleSignInClient;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewBinding();
        initCLick();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");

        //Facebook login
        callbackManager = CallbackManager.Factory.create();

    }

    private void googleLogin()
    {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(this, gso);
    }
    private void initViewBinding()
    {
        loginSignupPageBinding = LoginSignupPageBinding.inflate(getLayoutInflater());
        setContentView(loginSignupPageBinding.getRoot());
    }

    private void initCLick()
    {

        loginSignupPageBinding.fbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                if (LoginManager.getInstance() != null)
                {
                    LoginManager.getInstance().logOut();
                }
                LoginManager.getInstance().setLoginBehavior(LoginBehavior.WEB_ONLY);
                LoginManager.getInstance().logInWithReadPermissions(LoginAndSignup.this, Arrays.asList( "email", "public_profile"));
                LoginManager.getInstance().registerCallback(
                        callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                progressDialog.dismiss();
                                CustomToast.success(LoginAndSignup.this,"Login Success");
                                GraphRequest.newMeRequest(
                                        loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                            @Override
                                            public void onCompleted(JSONObject me, GraphResponse response) {
                                                username = me.optString("name");
                                                PrefUtils.saveUsername(LoginAndSignup.this,username);
                                                finish();
                                            }
                                        }).executeAsync();
                                PrefUtils.saveIsLogin(LoginAndSignup.this,true);
                                String profileURL="https://graph.facebook.com/"+loginResult.getAccessToken().getUserId()+"/picture?type=large";
                                PrefUtils.saveUserProfilePicURL(LoginAndSignup.this,profileURL);

                            }

                            @Override
                            public void onCancel() {
                                progressDialog.dismiss();
                                CustomToast.defaultShort(LoginAndSignup.this,"Login Cancelled");
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                progressDialog.dismiss();
                                if (exception instanceof FacebookAuthorizationException) {
                                    if (AccessToken.getCurrentAccessToken() != null) {
                                        LoginManager.getInstance().logOut();
                                    }
                                }
                            }
                        }
                );
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
