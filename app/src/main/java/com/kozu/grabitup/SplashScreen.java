package com.kozu.grabitup;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.grabitup.dashboard.view.DashboardView;
import com.kozu.grabitup.databinding.SplashScreenBinding;
import com.kozu.grabitup.utility.PrefUtils;

public class SplashScreen extends AppCompatActivity {
    SplashScreenBinding splashScreenBinding;
    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (PrefUtils.getTheme(this)==1)
        {
            getTheme().applyStyle(R.style.MyDarkTheme, true);
        }else
        {
            getTheme().applyStyle(R.style.BaseTheme, true);
        }
        initViewBinding();

        splashScreenBinding.marquee.setSelected(true);
        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashScreen.this, DashboardView.class);
                startActivity(intent);
                finish();
            }
        },3000);


    }

    private void initViewBinding()
    {
        splashScreenBinding = SplashScreenBinding.inflate(getLayoutInflater());
        setContentView(splashScreenBinding.getRoot());
    }

}